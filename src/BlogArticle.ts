export interface BlogArticle {
    status: string
    title: string
    coordinates?: {
        lat: number,
        lng:number
    }
    isCompétence: boolean
    isBandeau: boolean
    ville: string
    ordre: number
    html: string
    template: string
    slug: string
    filePath: string
    illustration: string | undefined
    dateDécision: Date | undefined
    tribunal: string
}

export interface DécisionBlogArticle extends BlogArticle{
    dateDécision: Date
}