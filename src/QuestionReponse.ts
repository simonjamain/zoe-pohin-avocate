export interface QuestionReponse {
    question: string
    reponseHtml: string
    theme: string
}