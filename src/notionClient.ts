import { Client } from "@notionhq/client";
import { BlockObjectResponse, GetPageResponse, PageObjectResponse, PartialBlockObjectResponse, PartialPageObjectResponse } from "@notionhq/client/build/src/api-endpoints";
import { config } from "dotenv";
import { apiCallDelay } from "./helper";
import { simpleCache } from "./simpleCache";

config()
config({ path: `.env.local`, override: true })

const notion = new Client({
  auth: process.env.NOTION_API_KEY,
});

export const getDatabase = async (databaseId: string) => {
  if (process.env.USE_FETCH_CACHE === 'true') {
    return simpleCache.get(
      `notion-database-${databaseId}`,
      async () => {
        const response = await notion.databases.query({
          database_id: databaseId,
        });

        await apiCallDelay()
        return response.results
      }
    )
  }

  const response = await notion.databases.query({
    database_id: databaseId,
  });

  await apiCallDelay()
  return response.results
};

export const getPage = async (pageId: string) => {
  if (process.env.USE_FETCH_CACHE === 'true') {
    return simpleCache.get(
      `notion-page-${pageId}`,
      async () => {
        const response = await notion.pages.retrieve({ page_id: pageId });

        await apiCallDelay()
        return response
      }
    )
  }

  const response = await notion.pages.retrieve({ page_id: pageId });

  await apiCallDelay()
  return response
};

export const getBlocks = async (blockId: string) => {
  if (process.env.USE_FETCH_CACHE === 'true') {
    return simpleCache.get(
      `notion-blocks-${blockId}`,
      async () => {
        const response = await notion.blocks.children.list({
          block_id: blockId,
        });

        await apiCallDelay()
        return response.results
      }
    )
  }


  const response = await notion.blocks.children.list({
    block_id: blockId,
  });

  await apiCallDelay()
  return response.results
};