
export async function apiCallDelay() {
    return new Promise<void>((resolve) => {
        setTimeout(() => resolve(), Number(process.env.NOTION_REQUEST_DELAY_MS as string))
    })
}