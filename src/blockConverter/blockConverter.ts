import { Client, isFullBlock } from '@notionhq/client';
import { v4 as uuidv4 } from 'uuid';
import * as https from 'https'
import { BlockObjectResponse, PartialBlockObjectResponse, RichTextItemResponse, getBlock } from '@notionhq/client/build/src/api-endpoints';
import { getBlocks } from '../notionClient';
import * as fs from 'fs';
import util from 'util'
import path from 'path';
import mime from 'mime-types';
import * as url from 'url';
import { apiCallDelay } from '../helper';
export class BlockConverter {
    public static async blocksToHtml(blocksObjectsResponse: Array<PartialBlockObjectResponse | BlockObjectResponse>): Promise<string> {
        let html = ""

        for (const blockObjectResponse of blocksObjectsResponse) {
            html += await this.blockToHtml(blockObjectResponse)
        }

        return html
    }

    public static async blockToHtml(blockObjectResponse: BlockObjectResponse | PartialBlockObjectResponse): Promise<string> {

        if (!isFullBlock(blockObjectResponse)) {
            throw new Error("partial block object");
        }

        const block = blockObjectResponse
        // const block = await this.notionClient.blocks.retrieve(
        //     {
        //         block_id: blockResponse.id,
        //     }
        //     )


        // const { type, id } = block.object;
        // const value = block[type];

        switch (block.type) {
            case 'toggle':
                {
                    const mainTag = `<h1 style="margin-top:0;">${this.richTextsToHtml(block.toggle.rich_text)}</h1>`
                    if (block.has_children) {
                        const childrenHtml = await this.getChildrenContent(block)
                        return `<section class="panneau-info">${mainTag}${childrenHtml}</section>`
                    }
                    return mainTag
                }
            case 'heading_1':
                {
                    const mainTag = `<h1>${this.richTextsToHtml(block.heading_1.rich_text)}</h1>`
                    if (block.has_children) {
                        const childrenHtml = await this.getChildrenContent(block)
                        return `<div class="panneau-info">${mainTag}${childrenHtml}</div>`
                    }
                    return mainTag
                }
            case 'heading_2':
                {
                    const mainTag = `<h2>${this.richTextsToHtml(block.heading_2.rich_text)}</h2>`
                    if (block.has_children) {
                        const childrenHtml = await this.getChildrenContent(block)
                        return `<div class="panneau-info">${mainTag}${childrenHtml}</div>`
                    }
                    return mainTag
                }
            case 'heading_3':
                {
                    const mainTag = `<h3>${this.richTextsToHtml(block.heading_3.rich_text)}</h3>`
                    if (block.has_children) {
                        const childrenHtml = await this.getChildrenContent(block)
                        return `<div class="panneau-info">${mainTag}${childrenHtml}</div>`
                    }
                    return mainTag
                }
            case 'paragraph':
                return `<p>${this.richTextsToHtml(block.paragraph.rich_text)}</p>`
            case 'bulleted_list_item':
                return `<li>${this.richTextsToHtml(block.bulleted_list_item.rich_text)}</li>`
            case 'numbered_list_item':
                return `<li>${this.richTextsToHtml(block.numbered_list_item.rich_text)}</li>`
            case 'quote':
                return `<blockquote><p>${this.richTextsToHtml(block.quote.rich_text)}</p></blockquote>`
            case 'image':
                {
                    const imageTemporaryUrl = block.image.type === 'external' ?
                        block.image.external.url
                        :
                        block.image.file.url
                    const localImagePath = await this.downloadFileFromUrl(imageTemporaryUrl, 'downloads')
                    return `<img style="width:90%;margin:5%;aspect-ratio: 1 / 1;" src="${localImagePath}" />`
                }
            case 'file':
                {
                    const fileTemporaryUrl = block.file.type === 'external' ?
                        block.file.external.url
                        :
                        block.file.file.url
                    if (fileTemporaryUrl === '') {
                        console.log('lien de fichier vide')
                        return ''
                    }
                    const localFilePath = await this.downloadFileFromUrl(fileTemporaryUrl, 'downloads')
                    const fileName = path.parse((new url.URL(fileTemporaryUrl)).pathname).name.replaceAll('_',' ')
                    return `<p class="file-link"><a href="${localFilePath}">${fileName} <i class="fa-solid fa-arrow-down"></i></a></p>`
                }
            case 'column_list':
                {
                    const childrenHtml = await this.getChildrenContent(block)
                    return `<div style="display:flex;flex-wrap: wrap;gap: 1em;margin:1em 0;">${childrenHtml}</div>`
                }
            case 'column':
                {
                    const childrenHtml = await this.getChildrenContent(block)
                    return `<div style="flex:1;min-width:300px;">${childrenHtml}</div>`
                }
            case 'table':
                {
                    const rowsHtml = await this.getChildrenContent(block)
                    return `<table>${rowsHtml}</table>`
                }
            case 'table_row':
                {
                    let tableCellsHtml = ''

                    block.table_row.cells.forEach((cellRichTexts) => {
                        tableCellsHtml += `<td>${this.richTextsToHtml(cellRichTexts)}</td>`
                    })

                    return `<tr>${tableCellsHtml}</tr>`
                }
            case 'callout':
                {
                    return `<p id="cta-contact" style="text-align: center; margin: 3em 0"><a class="cta-button" href="/contact.html"><span>${this.richTextsToHtml(block.callout.rich_text)}</span></a></p>`
                }
            default:
                console.log('unsupported block', util.inspect(block, false, null, true /* enable colors */))
                return `<!-- unsupported block :${block.type} -->`
        }
    }

    private static async getChildrenContent(block: BlockObjectResponse) {
        if (block.has_children) {
            const childrenBlocks = await getBlocks(block.id)
            return await this.blocksToHtml(childrenBlocks)
        }
        return ''
    }

    private static richTextToHtml(richText: RichTextItemResponse) {
        // heavily inspired by : https://samuelkraft.com/blog/building-a-notion-blog-with-public-api
        const classes = [
            richText.annotations.bold ? "bold" : "",
            richText.annotations.code ? "code" : "",
            richText.annotations.code ? "code" : "",
            richText.annotations.italic ? "italic" : "",
            richText.annotations.strikethrough ? "strikethrough" : "",
            richText.annotations.underline ? "underline" : "",
        ].join(" ")
        return `<span class="${classes}">${richText.plain_text}</span>`
    }

    private static richTextsToHtml(richTexts: Array<RichTextItemResponse>) {
        return richTexts.reduce((html, richText) => html + this.richTextToHtml(richText), '')
    }

    public static async downloadFileFromNotionFile(file: any, outputDir:string) {
        const fileTemporaryUrl = file.type === 'external' ?
        file.external.url
        :
        file.file.url
        if (fileTemporaryUrl === '') {
            console.log('lien de fichier vide')
            return undefined
        }
        return await this.downloadFileFromUrl(fileTemporaryUrl, outputDir)
    }

    /**
     * 
     * @returns a url pointing to the local downloaded file
     */
    public static async downloadFileFromUrl(fileUrl: string, outputDir:string):Promise<string> {
        if (process.env.USE_FETCH_CACHE === 'true') {
            console.info(`fichier ${fileUrl} ignoré car caching activé`)
            return '#' // if caching enabled (usually for dev), don't bother with file download
        }
        await apiCallDelay()
        return new Promise((resolve, reject) => {
            const parsedUrl = new url.URL(fileUrl);
            const fileExtension = this.getFileExtensionFromUrl(parsedUrl.pathname);
            const outputFilename = `${outputDir}/${path.parse(parsedUrl.pathname).name}-${uuidv4()}.${fileExtension}`;
            const fileStream = fs.createWriteStream('public/' + outputFilename);
        
            https.get(parsedUrl.href, (response) => {
              if (response.statusCode !== 200) {
                reject(new Error(`Failed to download file. Status code: ${response.statusCode}`));
                return;
              }
        
              response.pipe(fileStream);
        
              fileStream.on('finish', () => {
                fileStream.close();
                resolve('/' + outputFilename);
              });
        
              fileStream.on('error', (error) => {
                fs.unlinkSync('public/' + outputFilename);
                reject(error);
              });
            }).on('error', (error) => {
              reject(error);
            });
          });
    }
      
    public static getFileExtensionFromUrl(filePath :string) {
        const mimeType = mime.lookup(filePath)
        if(mimeType === false){
            throw new Error("type mime inconnu");
            
        }
        return mime.extension(mimeType)
      }
}