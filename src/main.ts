import { BlogArticle, DécisionBlogArticle } from './BlogArticle';
import { Client, isFullBlock, isFullPage } from '@notionhq/client'
import { BlockObjectResponse, ListDatabasesResponse } from '@notionhq/client/build/src/api-endpoints';
import { config } from 'dotenv'
import ejs from 'ejs'
import fs from 'fs'
import util from 'util'
import path from 'path'
import { BlockConverter } from './blockConverter/blockConverter';
import { getBlocks, getDatabase } from './notionClient';
import slugify from 'slugify'
import { QuestionReponse } from './QuestionReponse';
import moment from 'moment';

moment.locale('fr')
console.info(moment(new Date('2024-12-12')).format('MMMM YYYY'))

config()
config({ path: `.env.local`, override: true })

const faqThemesOrder = ["Conduite sous stupéfiants","Alcool au volant", "Excès de vitesse", "Convocations devant le Tribunal","Convocations devant le Tribunal", "Contraventions", "Annulation du permis", "Accidents de la route"]

// encode the email to to prevent spam bots
const emailBase64 = Buffer.from(process.env.CONTACT_EMAIL as string, 'utf8').toString('base64')

  ; (async () => {

    let faqThemesWithQuestions: Map<String,QuestionReponse[]> = await getFaqQuestionsResponses()
    faqThemesWithQuestions = new Map([...faqThemesWithQuestions.entries()].sort((themeA, themeB)=>{
      return faqThemesOrder.indexOf(themeA[0] as string) - faqThemesOrder.indexOf(themeB[0] as string)
    }))

    const blogArticlesDatabaseId = process.env.NOTION_BLOG_ARTICLES_DATABASE_ID as string
    let notionBlogArticles = await getDatabase(blogArticlesDatabaseId)

    const publishedBlogArticles: Array<BlogArticle> = []
    const geolocalizedBlogArticles: Array<BlogArticle> = []
    const bandeauBlogArticles: Array<BlogArticle> = []
    let compétencesBlogArticles: Array<BlogArticle> = []
    let décisionsBlogArticles: Array<DécisionBlogArticle> = []
    
    // todo désactiver pour prod
    // notionBlogArticles = notionBlogArticles.filter(
    //   (blogArticle) => {
    //     return blogArticle.id === "008b014258b64e25906b560deb57654e"
    //   }
    // )


    for await (const notionBlogDbPage of notionBlogArticles) {
      if (!isFullPage(notionBlogDbPage)) {
        console.log('notionBlogDbPage in not fullpage', notionBlogDbPage)
        throw new Error("partial page object");
      }

      const notionBlogPageBlocks = await getBlocks(notionBlogDbPage.id)

      const rawDateDecision = (notionBlogDbPage.properties["date de la décision"] as any).date?.start
      // console.log(util.inspect(notionBlogDbPage, false, null, true /* enable colors */))
      const blogArticle = {
        status: (notionBlogDbPage.properties.Status as any).status.name,
        title: (notionBlogDbPage.properties.Name as any).title[0].plain_text,
        isCompétence: (notionBlogDbPage.properties['Compétence'] as any).checkbox,
        isBandeau: (notionBlogDbPage.properties.Bandeau as any).checkbox,
        ville: (notionBlogDbPage.properties.Ville as any).multi_select.name,
        coordinates: (notionBlogDbPage.properties.Latitude as any).number === null ? undefined : {
          lat: (notionBlogDbPage.properties.Latitude as any).number,
          lng: (notionBlogDbPage.properties.Longitude as any).number
        },
        ordre: (notionBlogDbPage.properties["ordre d'apparition"] as any).number === null ? 100000 : (notionBlogDbPage.properties["ordre d'apparition"] as any).number as number,
        html: await BlockConverter.blocksToHtml(notionBlogPageBlocks),
        template: 'N/A',
        slug: 'N/A',
        filePath: 'N/A',
        dateDécision: rawDateDecision === undefined ? undefined : new Date(rawDateDecision),
        tribunal: (notionBlogDbPage.properties.Tribunal as any).rich_text[0]?.plain_text ?? '',
      } as BlogArticle

      if((notionBlogDbPage.properties.Illustration as any).files.length > 0) {
        blogArticle.illustration = (await BlockConverter.downloadFileFromNotionFile((notionBlogDbPage.properties.Illustration as any).files[0], 'downloads')) as string | undefined
      }
      blogArticle.template = blogArticle.isCompétence ? 'competence.html.ejs' : (blogArticle.dateDécision !== undefined ? 'decision.html.ejs' : 'article.html.ejs')
      blogArticle.slug = slugify(blogArticle.title)
      blogArticle.filePath = `articles/${blogArticle.slug}.html`
      blogArticle.ordre = (blogArticle.ordre)

      if (blogArticle.status == 'Publié') {
        publishedBlogArticles.push(blogArticle)
        if (blogArticle.isBandeau) {
          bandeauBlogArticles.push(blogArticle)
        }
        if (blogArticle.isCompétence) {
          compétencesBlogArticles.push(blogArticle)
        }
        if (blogArticle.dateDécision !== undefined) {
          décisionsBlogArticles.push(blogArticle as DécisionBlogArticle)
        }
        if (blogArticle.coordinates !== undefined) {
          geolocalizedBlogArticles.push(blogArticle)
        }
      }
    }

    compétencesBlogArticles = compétencesBlogArticles.sort((a,b) => a.ordre - b.ordre)
    décisionsBlogArticles = décisionsBlogArticles.sort((a,b) => b.dateDécision.getTime()- a.dateDécision.getTime())

    // console.log('compétencesBlogArticles',util.inspect(compétencesBlogArticles, false, null, true /* enable colors */))

    // render static pages
    const pagesTemplateDir = 'src/templates/pages'
    fs.readdir(pagesTemplateDir, function (err, files) {
      files.forEach(function (templateFilename) {
        if (!templateFilename.endsWith('.ejs')) {
          return
        }
        ejs.renderFile(`${pagesTemplateDir}/${templateFilename}`, { moment, emailBase64, compétencesBlogArticles, décisionsBlogArticles, geolocalizedBlogArticles, faqThemesWithQuestions, rootPath: '' }, {}, function (err, renderedTemplate) {
          if(err){
            console.log(err)
          }
          const renderFilePath = `public/${path.parse(templateFilename).name}`

          fs.writeFileSync(renderFilePath, renderedTemplate);
          console.log(`${renderFilePath} généré.`)
        });
      })
    })

    // render blog articles
    const articlesTemplatesDir = 'src/templates/articles'
    for (const publishedBlogArticle of publishedBlogArticles) {
      // console.log("publishedBlogArticle", publishedBlogArticle)
      // console.log("template path", `${articlesTemplatesDir}/${publishedBlogArticle.template}`)
      ejs.renderFile(`${articlesTemplatesDir}/${publishedBlogArticle.template}`, { moment, emailBase64, article: publishedBlogArticle, rootPath: '../' }, {}, function (err, renderedTemplate) {
        if(err){
          console.log(err)
        }
        const renderFilePath = `public/${publishedBlogArticle.filePath}`
        fs.writeFileSync(renderFilePath, renderedTemplate);
        console.log(`${renderFilePath} généré.`)
      });
    }
  })()

async function getFaqQuestionsResponses(): Promise<Map<String,QuestionReponse[]>> {

    const notionQuestionsReponses = await getDatabase(process.env.NOTION_FAQ_ARTICLES_DATABASE_ID as string)

    // console.log('notionQuestionsReponses',util.inspect(notionQuestionsReponses, false, null, true /* enable colors */))

    const themesWithQuestionsReponses: Map<String,QuestionReponse[]> = new Map()

    for await (const notionQuestionResponse of notionQuestionsReponses) {
      if (!isFullPage(notionQuestionResponse)) {
        console.log('notionBlogDbPage in not fullpage', notionQuestionResponse)
        throw new Error("partial page object");
      }

      const notionQuestionReponseBlocks = await getBlocks(notionQuestionResponse.id)
      // console.log(util.inspect(notionBlogDbPage, false, null, true /* enable colors */))
      const questionReponse: QuestionReponse = {
        question: (notionQuestionResponse.properties.Nom as any).title[0].plain_text,
        reponseHtml: await BlockConverter.blocksToHtml(notionQuestionReponseBlocks),
        theme: (notionQuestionResponse.properties["Catégorie"] as any).select.name
      }

      if(!themesWithQuestionsReponses.has(questionReponse.theme)) {
        themesWithQuestionsReponses.set(questionReponse.theme, [])
      }

      themesWithQuestionsReponses.get(questionReponse.theme)?.push(questionReponse)
    }
    return themesWithQuestionsReponses
}
// ; (async () => {
//   const leCabinetPageId = process.env.NOTION_LECABINET_PAGE_ID as string
//   const response = await notion.pages.retrieve({
//     page_id: leCabinetPageId,
//   })

//   console.log(response)

// const blockId = process.env.NOTION_LECABINET_PAGE_ID as string
// const response = await notion.blocks.children.list({
//   block_id: blockId,
//   page_size: 50,
// })

// for (const block of response.results) {
//   const blockObjectResponse = block as BlockObjectResponse
//   switch ((blockObjectResponse).type) {
//     case "paragraph":
//       console.log(blockObjectResponse.paragraph)
//       break;

//     default:
//       break;
//   }
// }
// })()


//   console.log(response);




