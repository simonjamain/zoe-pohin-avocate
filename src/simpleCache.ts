import fs from 'fs'
const cacheFile = '.simpleCache'
export const simpleCache = {

    async get<ReturnType> (key: string, fetchFunction: ()=>ReturnType):Promise<ReturnType>{
        
        try{
            fs.readFileSync(cacheFile).toString()
        } catch(e){
            //cache has not been created
            fs.writeFileSync(cacheFile, '{}');
        }

        const cache = JSON.parse(fs.readFileSync(cacheFile).toString())

        if(cache[key] !== undefined){
            console.info(`${key} récupéré depuis le cache`)
            return cache[key] as ReturnType
        }

        cache[key] = await fetchFunction()
        fs.writeFileSync(cacheFile, JSON.stringify(cache));
        console.info(`${key} mis en cache`)

        return cache[key]
    }
}